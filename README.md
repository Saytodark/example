# Learning whit Git

## Install Git on Linux Ubuntu

Before installing git, you must install the following packages:
```
    $ sudo apt update
    $ sudo apt upgrade
```
then we run the  following the command to install git:
```
    $ sudo apt install git
```
In order to see which version of git we have installed we run the following:
```
    $ git --version
```
## Git Configuration 

Once the installation has successfully completed, the next thing to do is to set up the configuration details of the GitHub user. To do this use the following two commands by replacing "user_name" with your GitHub username and replacing "email_id" with your email-id you used to create your GitHub account.
```
    $ git config --global user.name "Your Name"
    $ git config --global user.email "youremail@domain.com"
```
